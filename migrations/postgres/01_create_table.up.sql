
CREATE TABLE IF NOT EXISTS "brand" (
  "id" uuid PRIMARY KEY,
  "title" varchar,
  "photo" varchar,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "category" (
  "id" uuid PRIMARY KEY,
  "parent_id" UUID REFERENCES "category"("id") ON DELETE CASCADE,
  "brand_id" uuid REFERENCES "brand"("id"),
  "title" varchar,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "product" (
  "id" uuid PRIMARY KEY,
  "photo" varchar,
  "title" varchar,
  "category_id" uuid REFERENCES "category"("id"),
  "brand_id" uuid REFERENCES "brand"("id"),
  "barcode" varchar,
  "price" decimal(10, 2),
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);

