package memory

import (
	"content_service/genproto/content_service"
	"content_service/packages/helper"
	"content_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *content_service.CreateProduct) (resp *content_service.ProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "product" (
				id,
				title,
				photo,
				category_id,
				brand_id,
				barcode,
				price,
				created_at,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, now(), now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Title,
		req.Photo,
		req.CategoryId,
		req.BrandId,
		req.Barcode,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &content_service.ProductPrimaryKey{Id: id.String()}, nil
}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *content_service.ProductPrimaryKey) (resp *content_service.Product, err error) {

	query := `
		SELECT
			id,
			title,
			photo,
			category_id,
			brand_id,
			barcode,
			price,
			created_at,
			updated_at
		FROM "product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		title       sql.NullString
		photo       sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		barcode     sql.NullString
		price       sql.NullFloat64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&title,
		&photo,
		&category_id,
		&brand_id,
		&barcode,
		&price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &content_service.Product{
		Id:         id.String,
		Title:      title.String,
		Photo:      photo.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		Barcode:    barcode.String,
		Price:      float32(price.Float64),
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *content_service.GetListProductRequest) (resp *content_service.GetListProductResponse, err error) {

	resp = &content_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			title,
			photo,
			category_id,
			brand_id,
			barcode,
			price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			title       sql.NullString
			photo       sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			barcode     sql.NullString
			price       sql.NullFloat64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&title,
			&photo,
			&category_id,
			&brand_id,
			&barcode,
			&price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &content_service.Product{
			Id:         id.String,
			Title:      title.String,
			Photo:      photo.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			Barcode:    barcode.String,
			Price:      float32(price.Float64),
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *ProductRepo) Update(ctx context.Context, req *content_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "product"
			SET
				title = :title,
				photo = :photo,
				category_id = :category_id,
				brand_id = :brand_id,
				barcode = :barcode,
				price = :price,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"title":       req.GetTitle(),
		"photo":       req.GetPhoto(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"barcode":     req.GetBarcode(),
		"price":       req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProductRepo) Delete(ctx context.Context, req *content_service.ProductPrimaryKey) error {

	query := `DELETE FROM "product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
