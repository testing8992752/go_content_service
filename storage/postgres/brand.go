package memory

import (
	"content_service/genproto/content_service"
	"content_service/packages/helper"
	"content_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BrandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) storage.BrandRepoI {
	return &BrandRepo{
		db: db,
	}
}

func (c *BrandRepo) Create(ctx context.Context, req *content_service.CreateBrand) (resp *content_service.BrandPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "brand" (
				id,
				title,
				photo,
				created_at,
				updated_at
			) VALUES ($1, $2, $3, now(), now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Title,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &content_service.BrandPrimaryKey{Id: id.String()}, nil
}

func (c *BrandRepo) GetByPKey(ctx context.Context, req *content_service.BrandPrimaryKey) (resp *content_service.Brand, err error) {

	query := `
		SELECT
			id,
			title,
			photo,
			created_at,
			updated_at
		FROM "brand"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		title     sql.NullString
		photo     sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&title,
		&photo,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &content_service.Brand{
		Id:        id.String,
		Title:     title.String,
		Photo:     photo.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *BrandRepo) GetAll(ctx context.Context, req *content_service.GetListBrandRequest) (resp *content_service.GetListBrandResponse, err error) {

	resp = &content_service.GetListBrandResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			title,
			photo,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "brand"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			title     sql.NullString
			photo     sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&title,
			&photo,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Brandes = append(resp.Brandes, &content_service.Brand{
			Id:        id.String,
			Title:     title.String,
			Photo:     photo.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *BrandRepo) Update(ctx context.Context, req *content_service.UpdateBrand) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "brand"
			SET
				title = :title,
				photo = :photo,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":    req.GetId(),
		"title": req.GetTitle(),
		"photo": req.GetPhoto(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BrandRepo) Delete(ctx context.Context, req *content_service.BrandPrimaryKey) error {

	query := `DELETE FROM "brand" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
