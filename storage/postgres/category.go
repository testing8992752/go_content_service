package memory

import (
	"content_service/genproto/content_service"
	"content_service/packages/helper"
	"content_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {
	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *content_service.CreateCategory) (resp *content_service.CategoryPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "category" (
				id,
				title,
				parent_id,
				brand_id,
				created_at,
				updated_at
			) VALUES ($1, $2, $3, $4, now(), now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Title,
		helper.NewNullString(req.ParentId),
		helper.NewNullString(req.BrandId),
	)

	if err != nil {
		return nil, err
	}

	return &content_service.CategoryPrimaryKey{Id: id.String()}, nil
}

func (c *CategoryRepo) GetByPKey(ctx context.Context, req *content_service.CategoryPrimaryKey) (resp *content_service.Category, err error) {

	query := `
		SELECT
			id,
			title,
			parent_id,
			brand_id,
			created_at,
			updated_at
		FROM "category"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		title     sql.NullString
		parent_id sql.NullString
		brand_id  sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&title,
		&parent_id,
		&brand_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &content_service.Category{
		Id:        id.String,
		Title:     title.String,
		ParentId:  parent_id.String,
		BrandId:   brand_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *content_service.GetListCategoryRequest) (resp *content_service.GetListCategoryResponse, err error) {

	resp = &content_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			title,
			parent_id,
			brand_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "category"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			title     sql.NullString
			parent_id sql.NullString
			brand_id  sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&title,
			&parent_id,
			&brand_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categorys = append(resp.Categorys, &content_service.Category{
			Id:        id.String,
			Title:     title.String,
			ParentId:  parent_id.String,
			BrandId:   brand_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *content_service.UpdateCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "category"
			SET
				title = :title,
				parent_id = :parent_id,
				brand_id = :brand_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"title":     req.GetTitle(),
		"parent_id": helper.NewNullString(req.GetParentId()),
		"brand_id":  req.GetBrandId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *CategoryRepo) Delete(ctx context.Context, req *content_service.CategoryPrimaryKey) error {

	query := `DELETE FROM "category" WHERE id = $1 `

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
