package storage

import (
	"content_service/genproto/content_service"
	"context"
)

type StorageI interface {
	CloseDB()
	Category() CategoryRepoI
	Brand() BrandRepoI
	Product() ProductRepoI
}

type BrandRepoI interface {
	Create(ctx context.Context, req *content_service.CreateBrand) (resp *content_service.BrandPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *content_service.BrandPrimaryKey) (resp *content_service.Brand, err error)
	GetAll(ctx context.Context, req *content_service.GetListBrandRequest) (resp *content_service.GetListBrandResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateBrand) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.BrandPrimaryKey) error
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *content_service.CreateCategory) (resp *content_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *content_service.CategoryPrimaryKey) (resp *content_service.Category, err error)
	GetAll(ctx context.Context, req *content_service.GetListCategoryRequest) (resp *content_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateCategory) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.CategoryPrimaryKey) error
}

type ProductRepoI interface {
	Create(ctx context.Context, req *content_service.CreateProduct) (resp *content_service.ProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *content_service.ProductPrimaryKey) (resp *content_service.Product, err error)
	GetAll(ctx context.Context, req *content_service.GetListProductRequest) (resp *content_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.ProductPrimaryKey) error
}
