package service

import (
	"content_service/config"
	"content_service/genproto/content_service"
	"content_service/grpc/client"
	"content_service/packages/logger"
	"content_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CategoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CategoryService {
	return &CategoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CategoryService) Create(ctx context.Context, req *content_service.CreateCategory) (resp *content_service.Category, err error) {

	i.log.Info("---CreateCategory------>", logger.Any("req", req))

	pKey, err := i.strg.Category().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCategory->Category->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Category().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) GetByID(ctx context.Context, req *content_service.CategoryPrimaryKey) (resp *content_service.Category, err error) {

	i.log.Info("---GetCategoryByID------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategoryByID->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) GetList(ctx context.Context, req *content_service.GetListCategoryRequest) (resp *content_service.GetListCategoryResponse, err error) {

	i.log.Info("---GetCategorys------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategorys->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) Update(ctx context.Context, req *content_service.UpdateCategory) (resp *content_service.Category, err error) {

	i.log.Info("---UpdateCategory------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Category().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByPKey(ctx, &content_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CategoryService) Delete(ctx context.Context, req *content_service.CategoryPrimaryKey) (resp *content_service.Emptys, err error) {

	i.log.Info("---DeleteCategory------>", logger.Any("req", req))

	err = i.strg.Category().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &content_service.Emptys{}, nil
}
