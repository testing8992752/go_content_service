package service

import (
	"content_service/config"
	"content_service/genproto/content_service"
	"content_service/grpc/client"
	"content_service/packages/logger"
	"content_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ProductService) Create(ctx context.Context, req *content_service.CreateProduct) (resp *content_service.Product, err error) {

	i.log.Info("---CreateProduct------>", logger.Any("req", req))

	pKey, err := i.strg.Product().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProduct->Product->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Product().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) GetByID(ctx context.Context, req *content_service.ProductPrimaryKey) (resp *content_service.Product, err error) {

	i.log.Info("---GetProductByID------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProductByID->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) GetList(ctx context.Context, req *content_service.GetListProductRequest) (resp *content_service.GetListProductResponse, err error) {

	i.log.Info("---GetProducts------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProducts->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) Update(ctx context.Context, req *content_service.UpdateProduct) (resp *content_service.Product, err error) {

	i.log.Info("---UpdateProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Product().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &content_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProductService) Delete(ctx context.Context, req *content_service.ProductPrimaryKey) (resp *content_service.Emptyp, err error) {

	i.log.Info("---DeleteProduct------>", logger.Any("req", req))

	err = i.strg.Product().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &content_service.Emptyp{}, nil
}
