package service

import (
	"content_service/config"
	"content_service/genproto/content_service"
	"content_service/grpc/client"
	"content_service/packages/logger"
	"content_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BrandService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedBrandServiceServer
}

func NewBrandService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *BrandService {
	return &BrandService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *BrandService) Create(ctx context.Context, req *content_service.CreateBrand) (resp *content_service.Brand, err error) {

	i.log.Info("---CreateBrand------>", logger.Any("req", req))

	pKey, err := i.strg.Brand().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateBrand->Brand->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Brand().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BrandService) GetByID(ctx context.Context, req *content_service.BrandPrimaryKey) (resp *content_service.Brand, err error) {

	i.log.Info("---GetBrandByID------>", logger.Any("req", req))

	resp, err = i.strg.Brand().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBrandByID->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BrandService) GetList(ctx context.Context, req *content_service.GetListBrandRequest) (resp *content_service.GetListBrandResponse, err error) {

	i.log.Info("---GetBrands------>", logger.Any("req", req))

	resp, err = i.strg.Brand().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBrands->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BrandService) Update(ctx context.Context, req *content_service.UpdateBrand) (resp *content_service.Brand, err error) {

	i.log.Info("---UpdateBrand------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Brand().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateBrand--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Brand().GetByPKey(ctx, &content_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BrandService) Delete(ctx context.Context, req *content_service.BrandPrimaryKey) (resp *content_service.Empty, err error) {

	i.log.Info("---DeleteBrand------>", logger.Any("req", req))

	err = i.strg.Brand().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &content_service.Empty{}, nil
}
