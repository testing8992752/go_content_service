package grpc

import (
	"content_service/config"
	"content_service/genproto/content_service"
	"content_service/grpc/client"
	"content_service/grpc/service"
	"content_service/packages/logger"
	"content_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	content_service.RegisterBrandServiceServer(grpcServer, service.NewBrandService(cfg, log, strg, srvc))
	content_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	content_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
